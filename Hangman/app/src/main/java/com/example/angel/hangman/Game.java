package com.example.angel.hangman;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.res.Resources;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


import android.widget.ImageView;

import java.util.Random;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

public class Game extends AppCompatActivity {
    private String[] words;
    private Random rand;
    private String currWord;
    private LinearLayout wordLayout;
    private TextView[] charViews; //array of text views for the letters in word

    private RelativeLayout letters;
    //private LetterAdapter ltrAdapt;

    //body part images
    private ImageView[] bodyParts;
    //number of body part
    private int numParts = 6;
    //current part, increments when wrong answers are chosen
    private int currPart;
    //number of characters in current word
    private int numChars;
    //number correctly guessed
    private int numCorr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        //request resources, read words, and store in words variable
        Resources res = getResources();
        words = res.getStringArray(R.array.words);//word array in XML

        rand = new Random();
        currWord = "";

        //layout for answers
        wordLayout = findViewById(R.id.word);


        bodyParts = new ImageView[numParts];
        bodyParts[0] = findViewById(R.id.head);
        bodyParts[1] = findViewById(R.id.body);
        bodyParts[2] = findViewById(R.id.arm1);
        bodyParts[3] = findViewById(R.id.arm2);
        bodyParts[4] = findViewById(R.id.leg1);
        bodyParts[5] = findViewById(R.id.leg2);
        playGame();
    }//end of onCreate

    private void playGame(){
        //play a new game
        String newWord = words[rand.nextInt(words.length)];//random word chosen from array

        //check to make sure same word isn't picked twice in a row if user
        //plays again after winning or losing
        while(newWord.equals(currWord)){
            newWord = words[rand.nextInt(words.length)];
        }
        currWord = newWord;

        //create a text view for each letter in selected word
        charViews = new TextView[currWord.length()];
        wordLayout.removeAllViews();//removes any text views from Word Layout

        for(int i = 0; i< currWord.length(); i++){
            charViews[i] = new TextView(this);
            charViews[i].setText(""+currWord.charAt(i));
            charViews[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f);
            charViews[i].setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT));
            charViews[i].setGravity(Gravity.CENTER);
            charViews[i].setTextColor(this.getResources().getColor(R.color.colorPrimaryDark));//Set text to match background to appear hidden
            charViews[i].setBackgroundResource(R.drawable.letter_background);

            wordLayout.addView(charViews[i]);
        }


        currPart = 0;
        numChars = currWord.length();
        numCorr = 0;

        //hide images when game begins
        for(int i = 0; i< numParts; i++){
            bodyParts[i].setVisibility(View.INVISIBLE);
        }
    }//end GamePlay

    public void letterPressed(View view){
        //when user presses letter
        String ltr = ((TextView)view).getText().toString();
        char letterChar = ltr.charAt(0);
        view.setEnabled(false);//disable button
        view.setBackgroundResource(R.drawable.letter_down);

        boolean isCorrect = false;
        for(int k = 0; k < currWord.length(); k++){
            if(currWord.charAt(k)==letterChar){
                isCorrect = true;
                numCorr++;
                charViews[k].setTextColor(Color.BLACK);
            }
        }

        //check win or lose status
        if(isCorrect){
            //correct
        }else if (currPart < numParts){//if wrong guesses is less than 6
            bodyParts[currPart].setVisibility(View.VISIBLE);
            currPart++;//number of wrong guesses
        }else{//the user lost

            //alert box
            AlertDialog.Builder loseMsg = new AlertDialog.Builder(this);
            loseMsg.setTitle("Sorry!");
            loseMsg.setMessage("You lost!\nThe answer was:\n\n"+currWord);


            loseMsg.setNegativeButton("Exit Game",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Game.this.finish();
                        }
                    });
            loseMsg.show();
        }

        if(numCorr == numChars){
            //user wins

            // Display Alert Dialog
            AlertDialog.Builder winBuild = new AlertDialog.Builder(this);
            winBuild.setTitle("Congratulations!!");
            winBuild.setMessage("You win!\n\nThe answer was:\n\n"+currWord);


            winBuild.setNegativeButton("Exit",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Game.this.finish();
                        }});

            winBuild.show();
        }

    }

}
